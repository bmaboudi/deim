import numpy as np
from dolfin import *
from mshr import *
import scipy as scp
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve
import time

set_log_level(50)

class random_field_circle(UserExpression):
	def set_params(self,model_params):
		self.rf1 = model_params['rf1']
		self.rf2 = model_params['rf2']
		self.num_rf1 = self.rf1.size
		self.num_rf2 = self.rf2.size
		self.y1 = model_params['y1']
		self.y2 = model_params['y2']
		self.a = np.zeros(len(self.y1))
		for i in range(self.a.size):
			self.a[i] = (self.y1[i] - self.y2[i])/2
		self.b = np.zeros(len(self.y2))
		for i in range(self.b.size):
			self.b[i] = -(self.y1[i] + self.y2[i])/2
		self.h1 = 3.0
		self.h2 = 1.0
		self.scale = 0.05
	def eval(self, values, x):
		if( x[1] + self.a[0]*x[0] + self.b[0] > 0 ):
			values[0] = self.h2
			for i in range(self.num_rf2):
				k = i+1
				values[0] += self.scale*self.rf2[i]*np.sin(k*x[0])*np.sin(k*x[1])/k
		else:
			values[0] = self.h1
			for i in range(self.num_rf1):
				k = i+1
				values[0] += self.scale*self.rf1[i]*np.sin(k*x[0])*np.sin(k*x[1])/k

class source_term(UserExpression):
	def eval(self, values, x):
		values[0] = 50*exp(-(pow(x[0] , 2) + pow(x[1], 2)) / 0.1)


def boundary(x):
	return x[0] < -1 + DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

class high_fidelity():
	def __init__(self, num_out = 100, num_modes=1):
		#domain = Rectangle(Point(-1,-1), Point(1,1))
		#self.mesh = generate_mesh(domain, 40)
		#mesh_file = File('mesh.xml')
		#mesh_file << self.mesh
		self.mesh = Mesh('mesh.xml')
		self.V = FunctionSpace(self.mesh, 'CG', 1)

		self.u0 = Constant(0.0)
		self.bc = DirichletBC(self.V, self.u0, boundary)

		self.u = TrialFunction(self.V)
		self.v = TestFunction(self.V)
		
		FEM_el = self.V.ufl_element()
		self.f = source_term(element = FEM_el)

		self.alpha = random_field_circle(element = FEM_el)
		self.num_out = num_out
		self.num_modes = num_modes

		# loading reduced basis matrices
		self.template_bilin = scp.sparse.load_npz('template_bilin.npz')
		rb_data = np.load('rb_data_1024.npz')

		self.rb_basis = rb_data['rb']
		self.rb_k = self.rb_basis.shape[1]
		self.deim_P = rb_data['deim_P']
		self.deim_U = rb_data['deim_U']
		self.deim_l = rb_data['deim_l']
		self.deim_k = self.deim_U.shape[1]

		temp = np.linalg.inv( self.deim_P.T@self.deim_U )
		self.mat_M = self.deim_U@temp

		self.deim_PT = csr_matrix(self.deim_P.T)

		#loading FEM dof for ROM
		dof_data = np.load('dof_map.npz')
		self.c_list = dof_data['c_list']
		self.idx_I = dof_data['idx_I']
		self.idx_J = dof_data['idx_J']
		self.vec_idx = dof_data['vec_idx']

		temp = [ c for c in cells(self.mesh)]
		self.cs = []
		for i in range(len(self.c_list)):
			self.cs.append(temp[self.c_list[i]])

		

		#vec = np.zeros_like(self.template_bilin.data)
		#vec[ l ] = 1.0

		#temp = csr_matrix.copy( self.template_bilin )
		#temp.data = vec
		#temp.eliminate_zeros()

	def set_params(self, model_params):
		self.alpha.set_params(model_params)

	def assemble_rhs(self):
		self.L = self.f*self.v*dx
		rhs = assemble(self.L)
		self.bc.apply(rhs)
		self.b = np.reshape(rhs.get_local(),[-1])
		self.b_red = self.rb_basis.T@self.b

	def construct_FEM_dof_map(self):
		I, J = self.template_bilin.nonzero()
		c_list = []
		idx_I = []
		idx_J = []
		vec_idx = []
		for iterator in range(self.deim_k):
			idx = self.deim_l[iterator]
			#print(idx)
			#print([I[idx],J[idx]])
			#print('here1')
			#print([I[idx], J[idx]])
			for c in cells(self.mesh):
				ver = self.V.dofmap().cell_dofs(c.index())
				temp = ver == I[idx]
				is_there = [i for i, x in enumerate(temp) if x]
				if( len(is_there) != 0 ):
					temp2 = ver == J[idx]
					is_there2 = [i for i, x in enumerate(temp2) if x]
					if( len(is_there2) != 0 ):
						vec_idx.append(iterator)
						c_list.append(c.index())
						idx_I.append(is_there[0])
						idx_J.append(is_there2[0])
		
		vec_idx = np.array(vec_idx,dtype='int32')
		c_list = np.array(c_list,dtype='int32')
		idx_I = np.array(idx_I,dtype='int32')
		idx_J = np.array(idx_J,dtype='int32')
		np.savez('dof_map.npz',c_list=c_list,idx_I=idx_I,idx_J=idx_J,vec_idx=vec_idx)
		print('data saved')

	def assemble_bilinear_reduced(self):
		vec = np.zeros(self.deim_k)
		self.a = self.alpha*self.u.dx(0)*self.v.dx(0)*dx + self.alpha*self.u.dx(1)*self.v.dx(1)*dx
		for i in range(self.vec_idx.shape[0]):
			value = assemble_local(self.a, self.cs[i])
			vec[self.vec_idx[i]] += value[self.idx_I[i],self.idx_J[i]]

		self.A_sp = self.template_bilin.copy()
		self.A_sp.data = vec
		temp = self.mat_M@vec
		self.A_sp.data = temp
		temp = self.A_sp.dot(self.rb_basis)
		self.bilin_red = self.rb_basis.T@temp

	def solve_red(self):
		sol_red = scp.linalg.solve(self.bilin_red,self.b_red)
		self.sol = self.rb_basis@sol_red

	def solve(self):
		self.sol = spsolve(self.A_sp, self.b)
		return self.sol

	def give_bilin_mat(self):
		return self.A_sp.data

	def save_solution(self):
		solution = Function(self.V)
		solution.vector().set_local(self.sol)
		file = File("solution.pvd")
		file << solution

	def save_random_field(self):
		alpha_func = interpolate(self.alpha, self.V)
		file = File('random_field.pvd')
		file << alpha_func

def find_lines(model_params):
	y1 = np.random.uniform(-1,1,1)
	y2 = np.random.uniform(-1,1,1)

	model_params['y1'] = y1
	model_params['y2'] = y2
	return model_params

if __name__ == '__main__':
	hf = high_fidelity(num_out=200)

	model_params = {}
	model_params = find_lines(model_params)
	model_params['rf1'] = np.random.uniform(0,1,10)
	model_params['rf2'] = np.random.uniform(0,1,10)

	hf.set_params(model_params)
	#hf.construct_FEM_dof_map()
	start = time.time()
	hf.assemble_rhs()
	hf.assemble_bilinear_reduced()
	hf.solve_red()
	end = time.time()
	print('elapsed time is {}'.format(end-start))
	hf.save_solution()
	hf.save_random_field()
