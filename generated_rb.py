import numpy as np
import scipy.linalg as linalg
import matplotlib.pyplot as plt
import argparse

class reduced_basis():
	def load_data(self,path):
		d = np.load(path)
		self.data = d['raw_out'].T
		self.bilin_data = d['raw_bilin'].T

	def compute_rb_basis(self,k):
		print('generating rb basis')
		U, S, V = linalg.svd(self.data)
		print('done')
		self.rb = []
		for i in range(len(k)):
			self.rb.append(U[:,:k[i]])
		#plt.semilogy(S)
		#plt.show()

	def deim(self,k):
		print('generating DEIM basis')
		U, S, V = linalg.svd(self.bilin_data)
		print('done')

		self.deim_U = []
		self.P = []
		self.l = []

		for idx in range(len(k)):
			self.deim_U.append( U[:,:k[idx]] )

			#d = np.load('rb_data.npz')
			#self.deim_U = d['deim_U']

			#plt.semilogy(S)
			#plt.show()

			deim_U = self.deim_U[idx]
			ndim = deim_U.shape[0]
			l = []
			temp = np.zeros(ndim)
			p = np.argmax( np.abs(deim_U[:,0]) )
			temp[p] = 1.0
			l.append(p)
			P = np.reshape(temp,[ndim,1])
			counter = 1

			for i in range(1,deim_U.shape[1]):
				temp_U = deim_U[:,:i]
				u = np.reshape(deim_U[:,i],[-1,1])
				c = linalg.solve( P.T@temp_U , P.T@u )
				r = u - temp_U@c
				p = np.argmax( np.abs(r) )

				temp = np.zeros([ndim,1])
				temp[p] = 1.0
				l.append(p)
				P = np.concatenate([P,temp], axis=1)

			self.P.append( P )
			self.l.append( l )

	def test_rom(self,path):
		d = np.load(path)
		print(d.files)

	def save_rb_mats(self,k):
		for i in range(len(k)):
			path = 'rb_data_%d.npz'%k[i]
			np.savez(path,rb=self.rb[i],deim_U=self.deim_U[i], deim_P=self.P[i], deim_l=self.l[i])

	def load_rb_mats(self,k):
		path = 'rb_data_%d.npz'%k
		d = np.load(path)
		self.rb = d['rb']
		self.deim_U = d['deim_U']
		self.P = d['deim_P']
		self.l = d['deim_l']

	def test_accuracy(self,path):
		d = np.load(path)
		test_data = d['raw_out'].T
		appr_data = self.rb@( self.rb.T@test_data )
		er2 = np.power( test_data - appr_data, 2 )
		er = np.sqrt( np.sum( er2, axis=0 ) )
		plt.plot(er)
		plt.show()


def generated_rb(k):
	rb = reduced_basis()
	rb.load_data('raw_train.npz')
	rb.compute_rb_basis(k)
	rb.deim(k)
	rb.save_rb_mats(k)

def test_rb(path_test,k):
	rb = reduced_basis()
	rb.load_rb_mats(k)
	rb.test_accuracy(path_test)

def command_organizer():
	parser = argparse.ArgumentParser()
	parser.add_argument('-o', '--operation', help='Operation: train or test', required=True)
	parser.add_argument('-k', '--k', help='RB dimension', required=False, nargs='+')
	args = parser.parse_args()
	return args

if __name__ == '__main__':
	args = command_organizer()
	if(args.operation == 'train'):
		l = list(map(int, args.k))
		generated_rb( l )
	if(args.operation == 'test'):
		test_rb('raw_test.npz',int(args.k[0]))

	#k = 10
	#rb = reduced_basis()
	#rb.load_data('raw_train.npz')
	#rb.compute_rb_basis(k)
	#rb.deim(k)
	#rb.save_rb_mats('rb_data.npz')
