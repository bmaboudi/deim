import numpy as np
from dolfin import *
from mshr import *
import scipy as scp
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve
import time

set_log_level(50)

def output_indices(x):
	return (x[0]*x[0] + x[1]*x[1] <  2) and (x[0]*x[0] + x[1]*x[1] >  1 - 3e-3)

class random_field_circle(UserExpression):
	def set_params(self,model_params):
		self.rf1 = model_params['rf1']
		self.rf2 = model_params['rf2']
		self.num_rf1 = self.rf1.size
		self.num_rf2 = self.rf2.size
		self.y1 = model_params['y1']
		self.y2 = model_params['y2']
		self.a = np.zeros(len(self.y1))
		for i in range(self.a.size):
			self.a[i] = (self.y1[i] - self.y2[i])/2
		self.b = np.zeros(len(self.y2))
		for i in range(self.b.size):
			self.b[i] = -(self.y1[i] + self.y2[i])/2
		self.h1 = 3.0
		self.h2 = 1.0
		self.scale = 0.05
	def eval(self, values, x):
		if( x[1] + self.a[0]*x[0] + self.b[0] > 0 ):
			values[0] = self.h2
			for i in range(self.num_rf2):
				k = i+1
				values[0] += self.scale*self.rf2[i]*np.sin(k*x[0])*np.sin(k*x[1])/k
		else:
			values[0] = self.h1
			for i in range(self.num_rf1):
				k = i+1
				values[0] += self.scale*self.rf1[i]*np.sin(k*x[0])*np.sin(k*x[1])/k

class source_term(UserExpression):
	def eval(self, values, x):
		values[0] = 50*exp(-(pow(x[0] , 2) + pow(x[1], 2)) / 0.1)

def boundary(x):
	return x[0] < -1 + DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

class high_fidelity():
	def __init__(self, num_out = 100, num_modes=1):
		#domain = Rectangle(Point(-1,-1), Point(1,1))
		#self.mesh = generate_mesh(domain, 40)
		#mesh_file = File('mesh.xml')
		#mesh_file << self.mesh
		self.mesh = Mesh('mesh.xml')
		self.V = FunctionSpace(self.mesh, 'CG', 1)

		self.u0 = Constant(0.0)
		self.bc = DirichletBC(self.V, self.u0, boundary)

		self.u = TrialFunction(self.V)
		self.v = TestFunction(self.V)
		
		FEM_el = self.V.ufl_element()
		self.f = source_term(element = FEM_el)

		self.alpha = random_field_circle(element = FEM_el)
		self.num_out = num_out
		self.num_modes = num_modes

	def set_params(self, model_params):
		self.alpha.set_params(model_params)

	def assemble_rhs(self):
		self.L = self.f*self.v*dx
		rhs = assemble(self.L)
		self.bc.apply(rhs)
		self.b = np.reshape(rhs.get_local(),[-1])

	def assemble_bilinear_matrix(self):
		self.a = self.alpha*self.u.dx(0)*self.v.dx(0)*dx + self.alpha*self.u.dx(1)*self.v.dx(1)*dx

		A = assemble(self.a)
		self.bc.apply(A)
		A_mat = as_backend_type(A).mat()
		self.A_sp = csr_matrix(A_mat.getValuesCSR()[::-1], shape = A_mat.size)

	def solve(self):
		self.sol = spsolve(self.A_sp, self.b)
		return self.sol

	def give_bilin_mat(self):
		return self.A_sp.data

	def save_solution(self):
		solution = Function(self.V)
		solution.vector().set_local(self.sol)
		file = File("solution.pvd")
		file << solution

	def save_random_field(self):
		alpha_func = interpolate(self.alpha, self.V)
		file = File('random_field.pvd')
		file << alpha_func

def find_lines(model_params):
	y1 = np.random.uniform(-1,1,1)
	y2 = np.random.uniform(-1,1,1)

	model_params['y1'] = y1
	model_params['y2'] = y2
	return model_params

if __name__=='__main__':
	hf = high_fidelity(num_out=200)

	model_params = {}
	model_params = find_lines(model_params)
	model_params['rf1'] = np.random.uniform(0,1,10)
	model_params['rf2'] = np.random.uniform(0,1,10)
	#model_params['rf_exp'] = np.random.normal(0,1,2)
	#model_params['mode'] = [ np.random.uniform(0.0,10.0) ]

	hf.set_params(model_params)
	start = time.time()
	hf.assemble_rhs()
	hf.assemble_bilinear_matrix()
	hf.solve()
	end = time.time()
	print('elapsed time is {}'.format(end-start))
	#hf.save_solution()
	#hf.save_random_field()