import numpy as np
import numpy.linalg as la
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import argparse
import progressbar
import data_generation as data
import os.path
from torch.optim.lr_scheduler import ReduceLROnPlateau

class network(nn.Module):
    def __init__( self, input_size, output_size ):
        super().__init__()
        self.fc1 = nn.Linear(input_size, 2000)
        self.fc2 = nn.Linear(2000, 2000)
        self.fc3 = nn.Linear(2000, 2000)
        self.fc4 = nn.Linear(2000, 2000)
        #self.fc5 = nn.Linear(2000, 2000)
        #self.fc6 = nn.Linear(2000, 2000)
        #self.fc7 = nn.Linear(2000, 2000)
        self.fc5 = nn.Linear(2000, output_size)

    def forward( self, x ):
        x = F.relu( self.fc1(x) )
        x = F.relu( self.fc2(x) )
        x = F.relu( self.fc3(x) )
        x = F.relu( self.fc4(x) )
        #x = F.relu( self.fc5(x) )
        #x = F.relu( self.fc6(x) )
        #x = F.relu( self.fc7(x) )
        return self.fc5(x)

def train_model(model_path, nn_input_size, nn_output_size):
    path = 'raw_train.npz'
    train_data = data.data(path=path, batch_size=512)

    #path = 'test_data.npz'
    #test_data = data.data(0, 'blob', batch_size = 64, num_batches= 100)
    #test_data.load_data(path)

    model = network( nn_input_size, nn_output_size )

    e_local = list()
    if( os.path.isfile(model_path) == True ):
        model.load_state_dict(torch.load(model_path))

    device = torch.device('cpu')
    #optimizer = optim.SGD(model.parameters(), lr = 0.001)
    optimizer = optim.Adam(model.parameters(),lr=0.0002)
    #scheduler = ReduceLROnPlateau(optimizer, factor=0.3, patience=0, verbose=True )
    criterion = nn.MSELoss()

    max_iter = 50

    for i in range( 0, max_iter ):
        model.train()
        e_local.clear()

        for j in range( train_data.num_batches ):
            optimizer.zero_grad()

            y_ = model( train_data.torch_in[j] )

            y = train_data.torch_out[j]

            #loss = torch.norm(y - y_, 2)
            loss = criterion(y,y_)
            loss.backward()

            optimizer.step()
            e_local.append( loss.item() )
            #print(loss)

        train_data.shuffle()
        e_av = np.sum(e_local)/len( e_local )
        print("Epoch %04d loss: %.10f" % (i + 1, e_av ) )
        torch.save( model.state_dict(), model_path )
        #scheduler.step(e_av)

    test_data = data.data(path='raw_test.npz', batch_size=1)
    model.eval()
    for i in range(0,test_data.num_batches):
        y_ = model( test_data.torch_in[i] )
        y = test_data.torch_out[i]
        #loss = criterion(y,y_)
        e = y-y_
        e_np = e.detach().numpy()
        loss = criterion(y,y_)
        print([ np.max( np.abs(e_np) ), loss.item() ])
        input()


def test_model(model_path, nn_input_size, nn_output_size):
    path = 'train_data.npz'
    test_data = data.data(0, 'blob', batch_size = 100, num_batches= 1)


#    path = 'train_data.npz'
#    test_data = data.data(0, 'blob', batch_size = 50, num_batches= 20)

    test_data.load_data(path)

    model = network( nn_input_size, nn_output_size )
    model.load_state_dict(torch.load(model_path))

    y_ = model( test_data.torch_in[0] )
    y = test_data.torch_out[0]

    e = y - y_
    e_np = e.detach().numpy()
    e_np = e_np.T
    e_np = la.norm(e_np,ord=2,axis=0)
    print(e_np)
    print(np.sum(e_np)/e_np.size )



class network_eval():
    def __init__(self, model_path):
        self.model = network( 22, 2513 )
        self.model.load_state_dict(torch.load(model_path))
        self.model.eval()

    def eval_model(self, in_vec):
        out_vec = self.model( in_vec )
        return np.squeeze( np.asarray (out_vec.detach().numpy() ) )

def parse_commandline():
    parser = argparse.ArgumentParser(description='Decoder')
    parser.add_argument('-o','--operation',help='Operation', required=True)
    parser.add_argument('-mp','--model-path',help='Model Path', required=True)

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    sample_size = 500
    n_field_expansion = 0

    nn_input_size = 22
    nn_output_size = 2513

    train_model('model.pt', nn_input_size, nn_output_size)

    #args = parse_commandline()

    #if args.operation == 'train':
    #    train_model(args.model_path, sample_size, n_field_expansion, nn_input_size, nn_output_size)

    #if args.operation == 'test':
    #    test_model(args.model_path, nn_input_size, nn_output_size)
